#!/usr/bin/env python3

import argparse
import yaml

def parse_rules(yaml_rule_file=None, severity=None):
    stream = open(yaml_rule_file, 'r')
    data = yaml.load(stream)

    # all severities by default
    filter_severities = ['critical', 'warning']
    if not severity is None and severity in filter_severities:
        filter_severities = [severity]

    report = {}

    for document in data['items']:
        if document.get('kind') == 'PrometheusRule':
            spec = document.get('spec')
            groups = spec.get('groups')
            for group in groups:
                report[group.get('name')] = []
                for rule in group.get('rules'):
                    labels = rule.get('labels')
                    if labels and labels.get('severity') in filter_severities:
                        annotations = rule.get('annotations')
                        if annotations is None:
                            desc = 'N/A (no annotations)'
                        else:
                            desc = annotations.get('message')
                            if desc is None:
                                desc = annotations.get('summary')
                            elif desc is None:
                                desc = annotations.get('description')
                            elif desc is None:
                                desc = 'N/A'
                        report[group.get('name')].append(
                            {
                                'alert': rule.get('alert'),
                                'severity': labels.get('severity'),
                                'description': desc
                            }
                        )

    # print report
    for group_name, rules in report.items():
        if len(rules) == 0:
            continue
        print("Group name: {}".format(group_name))
        for rule in rules:
            print("* {} :: {} :: {}".format(
                rule.get('alert'), rule.get('severity'), rule.get('description'))
            )


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Parse rules")
    parser.add_argument('--yaml-rule-file', '-f',
                        required=True,
                        help='YAML file containing Prometheus rules')
    parser.add_argument('--severity', '-s',
                        required=False,
                        help='Severity')
    cli = parser.parse_args()

    parse_rules(cli.yaml_rule_file, cli.severity)
