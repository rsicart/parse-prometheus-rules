# Readable Prometheus rule list

Extracts Prometheus rules from Prometheus configuration in a readable text format.
Filtering by severity is possible (warning, critical).

1. Get prometheus rules

```
kubectl -nmonitoring get prometheusrules -oyaml > prometheus-operator-rules.yaml
```

2. Launch script

```
./parse_rules.py
usage: parse_rules.py [-h] --yaml-rule-file YAML_RULE_FILE
                      [--severity SEVERITY]
parse_rules.py: error: the following arguments are required: --yaml-rule-file/-f
```

```
./parse_rules.py -f ./prometheus-operator-rules.yaml -s critical
```
